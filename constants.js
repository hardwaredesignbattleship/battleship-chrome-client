const SERIAL_PATH_LINUX = ["/dev/ttyUSB1", 
                           "/dev/ttyUSB3"];
                           
const SERIAL_PATH_MAC_OS = ["/dev/tty.usbserial-210183711302B", 
                            "/dev/tty.usbserial-210183712703B"];

const TransmitCode = { 
  'TRANS_PLACE': 202,
  'TRANS_FINISH_PLACE': 203,
  'TRANS_ROLE_ATK': 204,
  'TRANS_ROLE_WAIT': 205,
  'TRANS_SWITCH_ROLE': 206,
  'TRANS_FINISH_ATTACK': 207,
  'TRANS_LOOSE': 208,
  'TRANS_WIN': 209
};

const States = {
  'INIT': 0,
  'PLACE': 1,
  'PLAYING': 2
};

const toTransmitBuffer = function(value8){
  var intBuffer = new Uint8Array(1);
  intBuffer[0] = value;
  return new ArrayBuffer(intBuffer);
};
const toTransmitValue = function(buffer){
  const intBuffer = new Uint8Array(buffer);
  return intBuffer[0];
};