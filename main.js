var gConnectionId = [-1, -1];

var gIsConnected = [false, false];

var gState = States.INIT;

var serialSearchConnect = function(serialPath, index){
  
  var connectCallback = function(info){
    if(info.connectionId) gConnectionId[index] = info.connectionId;
    console.log("Connect to Device");
    console.log(info);
    gIsConnected[index] = true;
    
    $("#connect-status-" + index)
        .removeClass()
        .addClass('info-text')
        .text('Connected to ' + serialPath);
        
    if(gIsConnected[0] === true && gIsConnected[1] === true){
      gState = States.PLACE;
      
      for(var i = 0; i < 2; i++){
        chrome.serial.send(gConnectionId[i], toTransmitBuffer(TransmitCode.TRANS_PLACE));
      }
    }
  };
  
  chrome.serial.getDevices(function(objArray){
    for(var i = 0; i < objArray.length; i++){
      var obj = objArray[i];
      
      console.log('Get Device:');
      console.log(obj);
      
      if(obj.path && obj.path.toString() == serialPath){
        chrome.serial.connect(serialPath, {
          "bitrate": 115200
        }, connectCallback);
        break;
      }
    }
  });
  
};

var doSerialConnect = function(index){
  var serialPath = $('#input-device-path-' + index).val();
  gConnectionId[index] = -1;
  gIsConnected[index] = false;
  
  $("#connect-status-" + index).text("");
  serialSearchConnect(serialPath, index);
  
  //Device connection timeout
  setTimeout(function(){
    if(gConnectionId[index] < 0){
      $("#connect-status-" + index)
        .removeClass()
        .addClass('error-text')
        .text('Error: Connect to ' + serialPath + ' failed');
    }
  }, 2000);
};

var gPlacementCounter = 0;

var serialReceiveCallback = function(info){
  var recvData = info.data;
  var index = (info.connectionId == gConnectionId[0])? 0 : 1;
  var op_index = (index === 0)? 1 : 0;
  
  var recvValue = toTransmitValue(recvData);
  
  switch(gState){
    case States.PLACE: {
      if(recvValue == TransCode.TRANS_FINISH_PLACE){
        gPlacementCounter++;
        if(gPlacementCounter >= 2){
          gState = States.PLAYING;
          
          chrome.serial.send(gConnectionId[0], toTransmitBuffer(TransmitCode.TRANS_ROLE_ATK));
          chrome.serial.send(gConnectionId[1], toTransmitBuffer(TransmitCode.TRANS_ROLE_WAIT));
        }
      }
      
      break;
    }
    
    case States.PLAYING: {
      if(recvValue == TransCode.TRANS_FINISH_ATTACK){
        for(var i =0; i < 2; i++){
          chrome.serial.send(gConnectionId[i], toTransmitBuffer(TransmitCode.TRANS_SWITCH_ROLE));
        }
      }else if(recvValue == TransCode.TRANS_LOOSE){
        chrome.serial.send(gConnectionId[op_index], toTransmitBuffer(TransmitCode.TRANS_WIN));
      }else{
        chrome.serial.send(gConnectionId[op_index], recvData);
      }
      
      break;
    }
  }
};

window.onload = function() {
  //console.log('Window OnLoad');
  
  //Setup device path
  chrome.runtime.getPlatformInfo(function(info){
    
    for(var i = 0; i < 2; i++){
      var serialPath = "";
      
      if(info.os == 'mac'){
        serialPath = SERIAL_PATH_MAC_OS[i];
      }else{
        serialPath = SERIAL_PATH_LINUX[i];
      }
      
      $('#input-device-path-' + i).val(serialPath);
      doSerialConnect(i);
    }
  });
  
  $("#but-reconnect-0").on('click', function(){
    doSerialConnect(0);
  });
  $("#but-reconnect-1").on('click', function(){
    doSerialConnect(1);
  });
  
  //Register serial receive callback
  chrome.serial.onReceive.addListener(serialReceiveCallback);
  chrome.serial.onReceiveError.addListener(function(info){
    console.log('Receive Error:');
    console.log(info);
  });
  
};
